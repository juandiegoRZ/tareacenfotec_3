# TareaCenfotec_3

## Instrucciones

### Ejercicios
1. Haciendo uso de smart pointers, cree una estructura llamada Cámara, Cámara tiene cantidad
de disparos, marca y año de fabricación.
    
    a) Cree una función que reciba como parámetro un puntero a la estructura Cámara. La
función verificará si la cantidad de disparos ha alcanzado el límite permitido, si es
así, entonces eliminará la estructura mediante reset.

    b) Mediante un ciclo ‘while’ aumente la cantidad de disparos de la Cámara y llame a la
función de verificación.

    c) Para salir del ciclo ‘while’, debe verificar que la cámara ya ha sido destruida, esto lo
puede verificar mediante el uso de un weak pointer.

2. Aplicación ToDo App

    ToDo App es una aplicación que permite al usuario agregar recordatorios, el recordatorio debe tener
lo siguiente:

    - Identificador
    - Nota
    - Fecha de recordatorio
    - Hora de recordatorio

    Ahora bien, para almacenar múltiples recordatorios, estos deben almacenarse en una lista enlazada.
Una lista enlazada es una estructura de datos que se utiliza para almacenar datos de manera
secuencial y dinámicamente. Leer el siguiente artículo para comprender mejor una lista enlazada:

    [What’s a Linked List, Anyway? [Part 1] | by Vaidehi Joshi | basecs | Medium
](https://medium.com/basecs/whats-a-linked-list-anyway-part-1-d8b7e6508b9d)

    Debe construir una lista para almacenar cada recordatorio, cada uno de los nodos de la lista tendrá
un recordatorio dentro. Ahora bien, la lista debe ser creada a partir de una struct con un puntero al
nodo anterior y un puntero al nodo siguiente, puede seguir el siguiente pseudocódigo:

        struct Node {
            smart_pointer<Nodo> previo;
            smart_pointer<Nodo> siguiente;
            smart_pointer<Recordatorio> nota;
        }

    De esta manera ToDo App tendrá un menú con el que el usuario puede:
    - Crear nuevo recordatorio.
    - Mostrar los recordatorios.
    - Mostrar un recordatorio por ID.
    - Eliminar un recordatorio por ID.
    - Salir
